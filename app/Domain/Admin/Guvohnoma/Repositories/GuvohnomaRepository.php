<?php

namespace App\Domain\Admin\Guvohnoma\Repositories;

use App\Domain\Admin\Guvohnoma\Models\Guvohnoma;

class GuvohnomaRepository
{
    public function getAll()
    {
        return Guvohnoma::all();
    }
}
