<?php

namespace App\Domain\Admin\Groups\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_title'=>'required',
            'course_id'=>'required',
            'start_date'=>'required',
            'end_date'=>'required',
            'teacher_one'=>'required',
            'teacher_two'=>'required',
            'organization'=>'required'
        ];
    }
}
