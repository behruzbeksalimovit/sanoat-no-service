<?php

namespace App\Domain\Admin\Groups\DTO;

use App\Domain\Admin\Groups\Models\Group;

class UpdateGroupDTO
{
    private string $group_title;
    private int $course_id;
    private string $start_date;
    private string $end_date;
    private string $teacher_one;
    private string $teacher_two;
    private string $organization;
    private Group $group;


    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setGroupTitle($data['group_title']);
        $dto->setCourseId($data['course_id']);
        $dto->setStartDate($data['start_date']);
        $dto->setEndDate($data['end_date']);
        $dto->setTeacherOne($data['teacher_one']);
        $dto->setTeacherTwo($data['teacher_two']);
        $dto->setOrganization($data['organization']);
        $dto->setGroup($data['group']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getGroupTitle(): string
    {
        return $this->group_title;
    }

    /**
     * @param string $group_title
     */
    public function setGroupTitle(string $group_title): void
    {
        $this->group_title = $group_title;
    }

    /**
     * @return int
     */
    public function getCourseId(): int
    {
        return $this->course_id;
    }

    /**
     * @param int $course_id
     */
    public function setCourseId(int $course_id): void
    {
        $this->course_id = $course_id;
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->start_date;
    }

    /**
     * @param string $start_date
     */
    public function setStartDate(string $start_date): void
    {
        $this->start_date = $start_date;
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->end_date;
    }

    /**
     * @param string $end_date
     */
    public function setEndDate(string $end_date): void
    {
        $this->end_date = $end_date;
    }

    /**
     * @return string
     */
    public function getTeacherOne(): string
    {
        return $this->teacher_one;
    }

    /**
     * @param string $teacher_one
     */
    public function setTeacherOne(string $teacher_one): void
    {
        $this->teacher_one = $teacher_one;
    }

    /**
     * @return string
     */
    public function getTeacherTwo(): string
    {
        return $this->teacher_two;
    }

    /**
     * @param string $teacher_two
     */
    public function setTeacherTwo(string $teacher_two): void
    {
        $this->teacher_two = $teacher_two;
    }

    /**
     * @return string
     */
    public function getOrganization(): string
    {
        return $this->organization;
    }

    /**
     * @param string $organization
     */
    public function setOrganization(string $organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return Group
     */
    public function getGroup(): Group
    {
        return $this->group;
    }

    /**
     * @param Group $group
     */
    public function setGroup(Group $group): void
    {
        $this->group = $group;
    }


}
