<?php

namespace App\Domain\Admin\Groups\Models;

use App\Domain\Admin\Courses\Models\Course;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;
    public function kurslar()
    {
        return $this->belongsTo(Course::class, 'course_id', 'id');
    }
}
