<?php

namespace App\Domain\Admin\Groups\Actions;

use App\Domain\Admin\Groups\DTO\StoreGroupDTO;
use App\Domain\Admin\Groups\Models\Group;
use Illuminate\Support\Facades\DB;

class StoreGroupAction
{
    public function execute(StoreGroupDTO $storeGroupDTO)
    {
//        dd(date("d.m.Y", strtotime($storeGroupDTO->getStartDate())));
        DB::beginTransaction();
        try {

            $groups = new Group();
            $groups->group_title = $storeGroupDTO->getGroupTitle();
            $groups->course_id = $storeGroupDTO->getCourseId();
            $groups->start_date = date("d.m.Y", strtotime($storeGroupDTO->getStartDate()));
            $groups->end_date = date("d.m.Y", strtotime($storeGroupDTO->getEndDate()));
            $groups->teacher_one = $storeGroupDTO->getTeacherOne();
            $groups->teacher_two = $storeGroupDTO->getTeacherTwo();
            $groups->organization = $storeGroupDTO->getOrganization();
            $groups->save();
        }catch (\Exception $exception)
        {
//            dd($exception);
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $groups;
    }
}
