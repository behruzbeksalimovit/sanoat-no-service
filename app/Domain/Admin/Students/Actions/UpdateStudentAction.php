<?php

namespace App\Domain\Admin\Students\Actions;

use App\Domain\Admin\Students\DTO\StoreStudentDTO;
use App\Domain\Admin\Students\DTO\UpdateStudentDTO;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage as StorageAlias;
use Intervention\Image\Facades\Image as InterventionImage;

class UpdateStudentAction
{
    public function execute(UpdateStudentDTO $updateStudentDTO)
    {

        DB::beginTransaction();
        try {
            $imageSizes = config('images.sizes');
            $students = $updateStudentDTO->getStudent();
            $students->student_fio = $updateStudentDTO->getStudentFio();
            $students->group_id = $updateStudentDTO->getGroupId();
            $students->malaka = $updateStudentDTO->getMalaka();
            $students->guvohnoma_id = $updateStudentDTO->getGuvohnomaId();
            $students->razryad = $updateStudentDTO->getRazryad();
            $students->pasport_seria = $updateStudentDTO->getPasportSeria();
            $students->pasport_number = $updateStudentDTO->getPasportNumber();
            $students->amaliy_baho = $updateStudentDTO->getAmaliyBaho();
            $students->nazariy_baho = $updateStudentDTO->getNazariyBaho();
            $students->profession = $updateStudentDTO->getProfession();

            if ($updateStudentDTO->getStudentImg() != null) {
                StorageAlias::delete($students->student_img);
//                //get filename with extension
//                $filenamewithextension = $updateStudentDTO->getStudentImg()->getClientOriginalName();
//
//                //get filename without extension
//                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
//
//                //get file extension
//                $extension = $updateStudentDTO->getStudentImg()->getClientOriginalExtension();
//
//                //filename to store
//                $filenametostore = $filename . '_' . uniqid() . '.' . $extension;
//                $updateStudentDTO->getStudentImg()->move('images/students', $filenametostore);
//
//                //Crop image here
//                $cropimage = public_path('images/students/') . $filenametostore;
//
//                InterventionImage::make($cropimage)->crop(request()->input('w'), request()->input('h'), request()->input('x1'), request()->input('y1'))->save($cropimage);
//
//                $students->student_img = $filenametostore;

                //get filename with extension
                $filenamewithextension = $updateStudentDTO->getStudentImg()->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $updateStudentDTO->getStudentImg()->getClientOriginalExtension();

                //filename to store
                $filenametostore = uniqid().'.' . $extension;
                $updateStudentDTO->getStudentImg()->move('images/students',$filenametostore);
                //Crop image here
                $cropimage = public_path('images/students/').$filenametostore;
                InterventionImage::make($cropimage)->crop(request()->input('w'), request()->input('h'), request()->input('x1'), request()->input('y1'))->save($cropimage);

//                StorageAlias::disk('google')->put('/'.$filenametostore, file_get_contents($cropimage));

//                $students->student_img = StorageAlias::disk('google')->url($filenametostore);
                $students->student_img = $filenametostore;
//                File::delete($cropimage);
            }

            $students->update();

        } catch (\Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $students;
    }
}
