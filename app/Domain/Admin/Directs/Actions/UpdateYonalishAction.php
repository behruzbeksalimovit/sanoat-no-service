<?php

namespace App\Domain\Admin\Directs\Actions;

use App\Domain\Admin\Directs\DTO\UpdateYonalishDTO;
use Illuminate\Support\Facades\DB;

class UpdateYonalishAction
{
    public function execute(UpdateYonalishDTO $updateYonalishDTO)
    {
        DB::beginTransaction();
        try {
            $yonalishs = $updateYonalishDTO->getYonalish();
            $yonalishs->plan_title = $updateYonalishDTO->getPlanTitle();
            $yonalishs->update();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $yonalishs;
    }
}
