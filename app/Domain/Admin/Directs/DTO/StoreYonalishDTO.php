<?php

namespace App\Domain\Admin\Directs\DTO;

class StoreYonalishDTO
{
    private string $plan_title;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setPlanTitle($data['plan_title']);
        return $dto;
    }
    /**
     * @return string
     */
    public function getPlanTitle(): string
    {
        return $this->plan_title;
    }

    /**
     * @param string $plan_title
     */
    public function setPlanTitle(string $plan_title): void
    {
        $this->plan_title = $plan_title;
    }



}
