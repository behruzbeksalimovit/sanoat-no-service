<?php

namespace App\Domain\Admin\Directs\DTO;

use App\Domain\Admin\Directs\Models\Yonalish;

class UpdateYonalishDTO
{
    private string $plan_title;
    private Yonalish $yonalish;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setPlanTitle($data['plan_title']);
        $dto->setYonalish($data['yonalish']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getPlanTitle(): string
    {
        return $this->plan_title;
    }

    /**
     * @param string $plan_title
     */
    public function setPlanTitle(string $plan_title): void
    {
        $this->plan_title = $plan_title;
    }

    /**
     * @return Yonalish
     */
    public function getYonalish(): Yonalish
    {
        return $this->yonalish;
    }

    /**
     * @param Yonalish $yonalish
     */
    public function setYonalish(Yonalish $yonalish): void
    {
        $this->yonalish = $yonalish;
    }


}
