<div class="sidebar-wrapper">
    <div>
        <div class="logo-wrapper"><a href="">
                    <span style="font-size: 20px" class="text-bg-info">СТ<span class="text-warning">Универсал</span></span>
                         <div class="back-btn"><i class="fa fa-angle-left"></i>
                         </div>
            <div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
            </a></div>
        <div class="logo-icon-wrapper"><a href="index.html"><img class="img-fluid" src="https://style.softgo.uz/assets/images/logo/logo-icon.png" alt=""></a></div>
        <nav class="sidebar-main">
            <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
            <div id="sidebar-menu">
                <ul class="sidebar-links" id="simple-bar">
                    <li class="back-btn"><a href="index.html"><img class="img-fluid" src="https://style.softgo.uz/assets/images/logo/logo-icon.png" alt=""></a>
                        <div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
                    </li>
                    <li class="sidebar-main-title">
                        <div>
                            <h6 class="">СТ Универсал</h6>
                            <p class="">Тизим бўлимлари</p>
                        </div>
                    </li>
                    <li class="sidebar-list ">
                        <a class="sidebar-link sidebar-title active" href="#">
                            <i data-feather="file-text"></i>
                            <span class="">Ўқув режа</span>
                        </a>
                        <ul class="sidebar-submenu {{(request()->is("dashboard/direction/create") || request()->is("dashboard/direction") || request()->is("dashboard/direction/*/edit")) ? "d-block" : ""}}">
                            <li><a target="_blank" class="" style="color: {{request()->is("dashboard/direction/create") ? "#7366ff" : ""}}" href="{{route("direction.create")}}">Ўқув режа яратиш</a></li>
                            <li><a target="_blank" class="" style="color: {{(request()->is("dashboard/direction") || request()->is("dashboard/direction/*/edit")) ? "#7366ff" : ""}}" href="{{route("direction.index")}}">Ўқув режалар</a></li>
                        </ul>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title {{ request()->is("/dashboard/course/create") ? "active" : "" }}" href="#"><i data-feather="briefcase"></i><span class="">Йўналишлар</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/course/create') || request()->is('dashboard/course') || request()->is('dashboard/course/*/edit') || request()->is('dashboard/course/groups/*')  || request()->is('dashboard/group/students/*')) ? 'd-block' : ''}}">
                            <li><a target="_blank" class="" style="color: {{request()->is('dashboard/course/create') ? '#7366ff' : ''}}" href="{{route('course.create')}}">Йўналишлар яратиш</a></li>
                            <li><a target="_blank" class="" style="color: {{(request()->is('dashboard/course') || request()->is('dashboard/course/*/edit') || request()->is('dashboard/course/groups/*') || request()->is('dashboard/group/students/*')) ? '#7366ff' : ''}}" href="{{route('course.index')}}">Йўналишлар рўйхати</a></li>
                        </ul>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="users"></i><span class="">Гуруҳлар</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/group/create') || request()->is('dashboard/group') || request()->is('dashboard/group/*/edit')) ? 'd-block' : ''}}">
                            <li><a target="_blank" class="" style="color: {{request()->is('dashboard/group/create') ? '#7366ff' : ''}}" href="{{route('group.create')}}">Гуруҳ яратиш</a></li>
                            <li><a target="_blank" class="" style="color: {{(request()->is('dashboard/group') || request()->is('dashboard/group/*/edit')) ? '#7366ff' : ''}}" href="{{route('group.index')}}">Гуруҳлар рўйхати</a></li>
                        </ul>
                    </li>
                    <li class="sidebar-list">
                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="user"></i><span class="">Тингловчилар</span></a>
                        <ul class="sidebar-submenu {{(request()->is('dashboard/student/create') || request()->is('dashboard/student') || request()->is('dashboard/student/*/edit')) ? 'd-block' : ''}}">
                            <li><a target="_blank" class="" style="color: {{request()->is('dashboard/student/create') ? '#7366ff' : ''}}" href="{{route('student.create')}}">Тингловчи яратиш</a></li>
                            <li><a target="_blank" class="" style="color: {{(request()->is('dashboard/student') || request()->is('dashboard/student/*/edit')) ? '#7366ff' : ''}}" href="{{route('student.index')}}">Тингловчилар руйхати</a></li>
                        </ul>
                    </li>
{{--                    <li class="sidebar-list">--}}
{{--                        <a class="sidebar-link sidebar-title" href="#"><i data-feather="sidebar"></i><span class="">Гувохномалар</span></a>--}}
{{--                        <ul class="sidebar-submenu {{(request()->is('dashboard/guvohnoma/create') || request()->is('dashboard/guvohnoma') || request()->is('dashboard/guvohnoma/*/edit')) ? 'd-block' : ''}}">--}}
{{--                            <li><a class="" style="color: {{request()->is('dashboard/guvohnoma/create') ? '#7366ff' : ''}}" href="{{route('guvohnoma.create')}}">Гувохнома яратиш</a></li>--}}
{{--                            <li><a class="" style="color: {{(request()->is('dashboard/guvohnoma') || request()->is('dashboard/guvohnoma/*/edit')) ? '#7366ff' : ''}}" href="{{route('guvohnoma.index')}}">Гувохномалар рўйхати</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
               </ul>
            </div>
            <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
        </nav>
    </div>
</div>
