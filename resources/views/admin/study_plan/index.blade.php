@extends('admin.layouts.master')
@section('title', 'Ўқув режалари')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>СТ Универсалда ўқув режалари жадвали</h5>
                        <span>Ушбу жадвалдан сиз таълим муассасидаги Ўқув режаларини кўришингиз, таҳрирлаш ишларини, шунингдек Ўқув режани базадан ўчириб ташлашингиз мумкин.</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive product-table">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="col-sm-3">ўқув режа номи</th>
                                    <th class="col-sm-3">ўқув режа яратилган вақти</th>
                                    <th>Таҳрирлаш</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($yonalishs as $yonalish)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td class="col-sm-3">
                                            {{$yonalish->plan_title}}
                                        </td>
                                        <td class="col-sm-3">
                                            {{$yonalish->created_at}}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{route('direction.edit', $yonalish)}}" style="padding: 6px 10px;"><i class="far fa-edit"></i></a>
                                            <form class="d-inline-block" method="post" action="{{route('direction.destroy', $yonalish)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger show_confirm" data-toggle="tooltip" title='Delete'><i class="far fa-trash-alt"></i></button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Ушбу ўқув режани ўчирмоқчимисиз?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
