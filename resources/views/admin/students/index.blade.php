@extends('admin.layouts.master')
@section('title', 'Тингловчилар жадвали')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>СТ Универсалда тингловчилар жадвали</h5>
                        <span>Ушбу жадвалдан сиз таълим муассасидаги тингловчиларни кўришингиз, таҳрирлаш ишларини, шунингдек тингловчини базадан ўчириб ташлашингиз мумкин.</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive product-table">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="col-sm-2">Тингловчи Ф.И.О</th>
                                    <th class="col-sm-3">Гуруҳ, Курси</th>
                                    <th class="col-sm-1">Тур</th>
                                    <th class="col-sm-1">Пасспорт</th>
                                    <th class="col-sm-2">QR-code</th>
                                    <th class="col-sm-2">Гувоҳнома</th>
                                    <th class="col-sm-2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $student)
                                    @if($student->active == 0)
                                      <tr style="color: #e3042f">
                                        <td>{{$loop->index+1}}</td>
                                        <td class="col-sm-2">
                                            <a target="_blank" href="{{route('show.info', $student)}}">{{$student->student_fio}}</a>
                                        </td>
                                        <td class="col-sm-3">
                                            {{$student->group_name->group_title." ( ".$student->malaka." ) ".$student->group_name->kurslar->showStudyPlan->plan_title}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$student->type_title->tur_title}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$student->pasport_seria."".$student->pasport_number}}
                                        </td>
                                        <td class="col-sm-2">

                                            {{   QrCode::size(200)
                                                   ->format('svg')
                                                    ->generate(route('show.info',$student), public_path('images/qrcodes/'.$student->qr_name))
                                            }}
                                            <img width="70" height="70" src="{{asset('images/qrcodes/'.$student->qr_name)}}" alt="">
                                        </td>
                                          <td class="col-sm-2">
                                              <a class="btn btn-warning" href="{{route('student.certi', $student)}}"><i class="fa fa-print"></i></a>
                                          </td>
                                        <td class="col-sm-2">
                                            <a class="btn btn-danger" href="{{route('student.noactive', $student)}}"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                    @else
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td class="col-sm-2">
                                                <a target="_blank" href="{{route('show.info', $student)}}">{{$student->student_fio}}</a>
                                            </td>
                                            <td class="col-sm-3">
                                                {{$student->group_name->group_title." ( ".$student->malaka." ) ".$student->group_name->kurslar->showStudyPlan->plan_title}}
                                            </td>
                                            <td class="col-sm-2">
                                                {{$student->type_title->tur_title}}
                                            </td>
                                            <td class="col-sm-1">
                                                {{$student->pasport_seria."".$student->pasport_number}}
                                            </td>
                                            <td class="col-sm-2">

                                                {{   QrCode::size(200)
                                                       ->format('svg')
                                                        ->generate(route('show.info',$student), public_path('images/qrcodes/'.$student->qr_name))
                                                }}
                                                <img width="70" height="70" src="{{asset('images/qrcodes/'.$student->qr_name)}}" alt="">
                                            </td>
                                            <td class="col-sm-2">
                                                  <a class="btn btn-warning" href="{{route('student.certi', $student)}}"><i class="fa fa-print"></i></a>
                                            </td>
                                            <td class="col-sm-2">
                                                <a class="btn btn-primary" href="{{route('student.edit', $student)}}"><i class="far fa-edit"></i></a>
                                                <a class="btn btn-success" href="{{route('student.active', $student)}}"><i class="fa fa-check"></i></a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Ushbu kursni o'chirmoqchimisiz?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
