@extends('admin.layouts.master')
@section('title', 'Йўналишни таҳрирлаш')
@section('content')
    <div class="card">

        <div class="card-body">
            <form class="" action="{{route('course.update', $course)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="course_title">Йўналиш номи</label>
                    <input value="{{$course->course_title}}" name="course_title" class="form-control @error('course_title') is-invalid @enderror"
                           type="text">
                    @error('course_title')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <div class="col-form-label">Ўқув режани танланг</div>
                    <select name="stude_plan_id" class="js-example-basic-single col-sm-12 @error('stude_plan_id') is-invalid @enderror">
                        @foreach($plans as $plan)
                            @if($plan->id == $course->stude_plan_id)
                                <option selected value="{{$plan->id}}">{{$plan->plan_title}}</option>
                            @else
                                <option value="{{$plan->id}}">{{$plan->plan_title}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('stude_plan_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="nazariy_soat">Назарий соат</label>
                    <input value="{{$course->nazariy_soat}}" name="nazariy_soat" class="form-control @error('nazariy_soat') is-invalid @enderror"
                           type="number" placeholder="Назарий соат">
                    @error('nazariy_soat')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="amaliy_soat">Амалий соат</label>
                    <input value="{{$course->amaliy_soat}}" name="amaliy_soat" class="form-control @error('amaliy_soat') is-invalid @enderror"
                           type="number" placeholder="Амалий соат">
                    @error('amaliy_soat')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Йўналишни таҳрирлаш</button>
                </div>
            </form>
        </div>
    </div>
@endsection
