<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<table style='width: 387px; height: 223.4px;' width='500px'>
    <tbody>
    <tr style='height: 110px;' valign='top'>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left: 4.5pt double #000000; border-right-style: none; padding: 0in 0in 0in 0.08in; height: 110px; width: 71.3333px;' width='96' height='79'>
            <p><img src='{{asset('admin/assets/images/medical/vodit.jpg')}}' alt='' width='72' height='54' /></p>
        </td>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left-style: none; border-right-style: none; padding: 0in; height: 110px; width: 218.97px;' width='200'>
            <p style='text-align: center;'><strong>&laquo;САНОАТ&nbsp; ТЕХНИКА</strong></p>
            <p style='margin-bottom: 0in; text-align: center;'><strong>УНИВЕРСАЛ</strong><strong>&raquo;</strong></p>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif; font-size: 12pt;'>нодавлат &nbsp;таълим муассасаси</span></p>
            <p style='text-align: center;'><span style='color: #0070c0;'><span style='font-family: sans-serif;'><span style='font-size: 12pt;'><strong>Гувоҳнома </strong></span></span></span><span style='color: #ff0000;'><span style='font-family: sans-serif;'><span style='font-size: 12pt;'><strong></strong></span></span></span><span style='color: #ff0000;'><span style='font-family: sans-serif;'><span style='text-decoration-line: underline;'><strong>№    {{sprintf("%04d", $student->number)}}   </strong></span></span></span></p>
        </td>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left-style: none; border-right: 6pt double #000000; padding: 0in 0.08in 0in 0in; height: 110px; width: 67.3458px;' width='116'>
            <p><img src='{{asset('admin/assets/images/medical/voditel.jpg')}}' alt='' width='68' height='46' /></p>
        </td>
    </tr>
    <tr style='height: 113.4px;' valign='top'>
        <td style='border-style: none double double; border-bottom-width: 4.5pt; border-bottom-color: #000000; border-left-width: 4.5pt; border-left-color: #000000; border-right-width: 6pt; border-right-color: #000000; padding: 0in 0.08in; height: 113.4px; width: 357.649px;' colspan='3' width='450' height='170'>
            <p style='margin-bottom: 0in; text-align: left;'><span style='font-weight: 400; font-size: 12pt;'>Ҳайдовчиси</span><span style='font-weight: 400;'>&nbsp;</span><strong><em>{{$student->student_fio}}</em></strong><span style='font-weight: 400;'>га <span style='font-size: 12pt;'>берилди</span></span></p>
            <p style='margin-bottom: 0in; text-align: justify;'>&nbsp;</p>
            <p style='margin-bottom: 0in;'><span style='font-family: sans-serif;'><em><strong>&nbsp; &nbsp; &nbsp;</strong></em></span><span style='font-family: sans-serif;'><em><span style='text-decoration-line: underline;'><strong></strong></span></em></span><span style='font-family: sans-serif;'><em></em></span><span style='font-family: sans-serif;'><em><span style='text-decoration-line: underline;'><strong></strong></span></em></span><span style='font-family: sans-serif;'><em><strong></strong></em></span><span style='font-family: sans-serif;'><em><span style='text-decoration-line: underline;'><strong>{{$student->group_name->end_date}}</strong></span></em></span><span style='font-family: sans-serif;'><em><strong> йил</strong></em></span><span style='font-family: sans-serif;'>&nbsp;</span></p>
        </td>
    </tr>
    </tbody>
</table>
<div style='break-before: page; clear: both; line-height: 0.5;'>&nbsp;</div>
<table style='width: 387px; height: 223.4px; margin-left: 379px;'>
    <tbody>
    <tr style='height: 231px;'>
        <td style='padding: 0in 0.08in; border: 1px solid #000000; height: 231px;' valign='top' width='434' height='296'>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='font-size: 12pt;'><strong> </strong></span></span></p>
            <p style='margin-left: 0.33in; margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='font-size: 10pt;'><em><strong style='font-size: 16px;'>&laquo;Мехнат мухофазаси техника ва йўл харакати хавфсизлиги&raquo;</strong></em></span></span><span style='font-family: sans-serif;'><span style='font-size: 12pt;'> б</span></span><span style='font-family: sans-serif;'><span style='font-size: 12pt;'>ў</span></span><span style='font-family: sans-serif;'><span style='font-size: 12pt;'>йича</span></span></p>
            <p style='margin-left: 0.33in; margin-bottom: 0in;'><span style='font-family: sans-serif;'><span style='font-size: 12pt;'><em><span style='text-decoration-line: underline;'><strong style='font-size: 16px;'>{{$student->group_name->start_date}}</strong></span></em></span></span><span style='font-family: sans-serif;'><span style='font-size: 12pt;'> йилдан</span></span></p>
            <p style='margin-left: 0.33in; margin-bottom: 0in;'><span style='font-family: sans-serif;'><span style='font-size: 12pt;'><em><span style='text-decoration-line: underline;'><strong style='font-size: 16px;'>{{$student->group_name->end_date}}</strong></span></em></span></span><span style='font-family: sans-serif;'><span style='font-size: 12pt;'> йилгача {{$student->group_name->kurslar->nazariy_soat + $student->group_name->kurslar->amaliy_soat}} соатлик машғулотларда иштирок этди.</span></span></p>
            <p style='margin-left: 0.33in; margin-bottom: 0in;'><span style='text-decoration-line: underline; font-size: 16px;'>№<span style='font-family: sans-serif;'><span style='font-size: 12pt;'><em><strong style='font-size: 16px;'>{{$student->group_name->group_title}}.   {{$student->group_name->end_date}} йил</strong></em></span></span></span></p>
            <p style='margin-left: 0.33in; margin-bottom: 0in;'><span style='font-family: sans-serif;'><span style='font-size: 12pt;'>Директор: __________</span></span></p>
            <p style='margin-left: 0.33in; margin-bottom: 0in;'><span style='font-family: sans-serif;'><span style='font-size: 10pt;'>Эслатма:</span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'> </span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'>Ушбу</span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'> </span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'> гуво</span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'>ҳ</span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'>нома</span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'> </span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'> транспорт</span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'> </span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'> воситасини </span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'>бошкариш х</span></span><span style='font-family: sans-serif;'><span style='font-size: 10pt;'>укукини бермайди</span></span></p>
        </td>
    </tr>
    </tbody>
</table>

</textarea>
<script src="https://cdn.tiny.cloud/1/tmyd5g5k45okdpvyfeaec7bygj130xsrfy65t4prftu16zt0/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_13_tur.css') }}"]
    });
</script>

</body>
</html>

