<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<main class='main'>
    <div class='row'>
        <p class='title'><strong style='font-size: 1em; font-family: sans-serif;'> СВИДЕТЕЛЬСТВО </strong> <strong class='ml' style='color: red; font-size: 1em; font-family: sans-serif;'> №   {{sprintf("%04d", $student->number)}}    </strong></p>
        <div class='row-2'>
            <p class='ml-auto ml' style='font-size: 1.2em; text-align: center; font-family: sans-serif;'>Выдано: <strong  style='font-size: 1em; font-family: sans-serif; '> {{$student->student_fio}} </strong> <br />в том, что он (а) с {{$student->group_name->start_date}} года по {{$student->group_name->end_date}} года обучался (лось) по профессии</p>
        </div>
        <p class='center'><strong style='font-size: 1.4em; font-family: sans-serif; line-height: 0px;'>{{$student->malaka}}</strong></p>
        <p class='center ' style='margin: 0px;'><span style='font-size: 13px; font-family: sans-serif; line-height: 15px;'> (наименование профессии) </span></p> <br>
        <p class='center' style='font-family: sans-serif; font-size: 1.3em; line-height: 20px;'> {{$student->group_name->kurslar->showStudyPlan->plan_title}} </p>
        <p class='center' style='margin: 0px;'><span style='margin: 0px; font-size: 13px; font-family: sans-serif; line-height: 10px;'> (форма обучения)</span></p>
        <p class='title-2' style='font-family: sans-serif; font-size: 1.2em;'>НОУ&laquo;Саноат техника универсал &raquo; </p>
        <p class='center' style='font-family: sans-serif; font-size: 1.2em;'>г.Бухара</p>
        <p class='center' style='margin: 0px; font-family: sans-serif;'><span style='font-size: 12px;'> (наименование предприятия организации) </span></p>
        <p class='center' style='font-size: 1.2em; font-family: sans-serif; text-align: justify;'>Прошел (а) полный курс теоретического обучения в объеме {{$student->group_name->kurslar->nazariy_soat}} часов и производственного обучения в объеме {{$student->group_name->kurslar->amaliy_soat}} часов и сдал (а) квалификационный экзамен с оценками:</p>
        <p><strong style='font-size: 1.2em; font-family: sans-serif;'>- теория: {{$student->nazariy_baho}}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; - практика: {{$student->amaliy_baho}}    </strong></p>

    </div>
    <div class='row' style='margin-left:26px'>
        <p class='center' ><b style='font-size: 1.1em; font-family: sans-serif;'> Решением (Экзаменационной)<br> Квалификационной комиссии <b></p>
        <p  class='center' style='font-size: 1.1em; font-family: sans-serif;'  > от {{$student->group_name->end_date}}</b>  г  Протокол № {{$student->group_name->group_title}} </p>
        <p  class='center title-2' style='font-family: sans-serif; font-size: 1.2em;' > <b style='font-size: 1.2em; font-family: sans-serif;' > гр   {{$student->student_fio}}</b> </p> <br>
        <p style='font-family: sans-serif; font-size: 1.1em;    '> Установлен тарифно-квалификационный разряд (класс категория) {{$student->razryad}}  <br>  по профессии:</p>
        <p class='center' ><b style='font-size: 1.3em; font-family: sans-serif; '>{{$student->malaka}}</b>  </p>
        <br>
        <p  ><b style='font-size: 1em; font-family: sans-serif; font-size: 1em;'>Председатель  квалификационной <br> комиссии: ______________________________</b></p>
        <br>
        <p  ><b style='font-size: 1em; font-family: sans-serif; font-size: 1em;'>Руководитель  предприятия <br> Организации: ___________________________</p>
        <br> <br>
        <p ><b  style='font-size: 17px !important; font-family: sans-serif; '>М.П.<br> НОУ«Саноат  техника  универсал <br> »    <b> </p>
        <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div>
    </div>
</main>


</textarea>
<script src="https://cdn.tiny.cloud/1/tmyd5g5k45okdpvyfeaec7bygj130xsrfy65t4prftu16zt0/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_3_tur.css') }}"]
    });
</script>

</body>
</html>
