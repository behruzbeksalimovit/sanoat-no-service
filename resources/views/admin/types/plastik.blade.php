<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<textarea id="cer">
        <div id='card_first'>
            <div class='d-f'>
                <img src='{{ asset("img/logo.svg") }}' alt='logo'>
                <h1>&nbsp;НОУ «Саноат техника &nbsp;&nbsp;&nbsp;универсал»</h1>
                <img src='{{ asset("images/qrcodes/".$student->qr_name) }}' alt='qr'>
            </div>

            <div class='bg' style='background: url({{asset('img/tbt.png')}})'>
                @if (file_exists(public_path('images/students/'.$student->student_img)))
                    <img style='width: 56px;height: 68px;margin-right: 5px;margin-left: 5px;margin-top: -5px;' src='{{ asset('images/students/'.$student->student_img) }}' alt='user'>
                @else
                    <img style='width: 56px;height: 68px;margin-right: 5px;margin-left: 5px;margin-top: -5px;' src='{{ $student->student_img }}' alt='user'>
                @endif
                <div class='txt'>
                    <h1>Удостоверение <span class='span_n'>№ {{sprintf("%04d", $student->number)}}</span></h1>
                    <h1 style='font-size:9px'>{{$student->student_fio}}</h1>
                    <h3 style=''>С {{$student->group_name->start_date}} г по {{$student->group_name->end_date}} г.</h3>
                    <h4 style=''>Обучался по професии <span>{{$student->malaka}}</span> в объеме {{$student->group_name->kurslar->nazariy_soat + $student->group_name->kurslar->amaliy_soat}} часов</h4>
                </div>
            </div>
        </div>
        <br><br>
        <div id='card_last'>
            <div class='d-f'>
                <img src='{{ asset("img/logo.svg") }}' alt='logo'>
                <h1>&nbsp;НОУ «Саноат техника &nbsp;&nbsp;&nbsp;универсал»</h1>
            </div>
            <div class='bg' style='background: url({{asset('img/tbt.png')}})'>
                <h2>Основание Протокола <span class='span_n'>№{{$student->group_name->group_title}}</span> <br> от. {{$student->group_name->end_date}} г.</h2>
{{--                <h1>{{$student->student_fio}}</h1>--}}
                <h3>{{$student->razryad}}</h3>
            </div>
            <div class='d-f'>
                <div class='txt'>
                    <a style='font-size:8px' href='#'>
                        <img style='width:10px; height:10px; margin:0; margin-right:2px;' src='{{ asset("img/globus.png") }}'/>
                        ntm-stu.uz
                    </a>
                    <a style='font-size:8px' href='#'>
                        <img style='width:10px; height:10px; margin:0; margin-right:2px;' src='{{ asset("img/call.png") }}'/>
                        +998914007555
                    </a>
                    <a style='font-size:8px' href='#'>
                        <img style='width:10px; height:10px; margin:0; margin-right:2px;' src='{{ asset("img/mail.png") }}'/>
                        sanoattexuniversal@mail.ru
                    </a>
                </div>
{{--                <img style='margin: 0;width:50px; height=50px;' src='{{ asset("images/qrcodes/".$student->qr_name) }}' alt='qr'>--}}
                <img style='margin-right:5px;width:50px; height=50px;'  src='{{ asset("images/locat.png") }}' alt='qr'>
            </div>
        </div>
</textarea>
<script src="https://cdn.tiny.cloud/1/tmyd5g5k45okdpvyfeaec7bygj130xsrfy65t4prftu16zt0/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/plastik.css') }}"]
    });
</script>

</body>
</html>
