<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<main class='main'>
    <div class='row'>
        <p class='title1' style='text-align:center;font-weight:bold'>НОУ«Саноат техника универсал» г.Бухара</p>
        <div class='row-2' >
            <div class='img'></div>
            <div class='ml-auto ml'>
                <p class='title3'  > <b class='title4'>УДОСТОВЕРЕНИЕ  </b> <b class='title4'  style='color: red'> №{{sprintf("%04d", $student->number)}}</b></p>
                <p class='font2' > Выдано: <b class='font2'>  {{$student->student_fio}} </b>
                    <br> в том, что он (а) с <br>  {{$student->group_name->start_date}}  г.   по {{$student->group_name->end_date}} г. </p>
            </div>
        </div>
        <p class='font'>прошел(а)    курсы  обучения в объеме ({{$student->group_name->kurslar->nazariy_soat + $student->group_name->kurslar->amaliy_soat}}) часов  по технике  безопасности персонала, работающего на высоте (верхолаз)  <p>

    </div>
    <div class='row'  >
        <p class='center'  ><b class='font2'> Основание протокола квалификационной комиссии  <b></p>
        <p  class='font2' >№{{$student->group_name->group_title}}  от {{$student->group_name->end_date}}</b>г. </p>
        <p  class='font2' > <b class='font2'>{{$student->student_fio}}</b> </p>
        <p class='font'> Прошёл курсы по технике безопасности персонала, работающего на высоте (верхолаз) </p>
        <br>
        <p  ><b class='font'>Председатель квалиф  <br> комиссии _______________________</b>  </p>
        <p  ><b class='font'>Директор учебного <br> заведения   _______________________ </b>    </p>
        <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div>

    </div>
</main>
</textarea>
<script src="https://cdn.tiny.cloud/1/tmyd5g5k45okdpvyfeaec7bygj130xsrfy65t4prftu16zt0/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_7_tur.css') }}"]
    });
</script>

</body>
</html>
