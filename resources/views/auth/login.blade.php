{{--@extends('layouts.app')--}}
{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Login') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('login') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <div class="form-check">--}}
{{--                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>--}}

{{--                                    <label class="form-check-label" for="remember">--}}
{{--                                        {{ __('Remember Me') }}--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-0">--}}
{{--                            <div class="col-md-8 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Login') }}--}}
{{--                                </button>--}}

{{--                                @if (Route::has('password.request'))--}}
{{--                                    <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                        {{ __('Forgot Your Password?') }}--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}
    <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Login</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="https://style.softgo.uz/styles/assets/img/favicon.png" rel="icon">
    <link href="https://style.softgo.uz/styles/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="https://style.softgo.uz/styles/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://style.softgo.uz/styles/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="https://style.softgo.uz/styles/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="https://style.softgo.uz/styles/assets/vendor/quill/quill.snow.css" rel="stylesheet">
    <link href="https://style.softgo.uz/styles/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
    <link href="https://style.softgo.uz/styles/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
    <link href="https://style.softgo.uz/styles/assets/vendor/simple-datatables/style.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="https://style.softgo.uz/styles/assets/css/style.css" rel="stylesheet">

    <!-- =======================================================
    * Template Name: NiceAdmin - v2.4.1
    * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<main>
    <div class="container">

        <section class="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6 d-flex flex-column align-items-center justify-content-center">

                        <div class="d-flex justify-content-center py-4">
                            <a href="" class="logo d-flex align-items-center w-auto">
                                <img src="https://style.softgo.uz/styles/assets/img/logo.png" alt="">
                                <span class="d-none d-lg-block">Sanoat Texnika Universal</span>
                            </a>
                        </div><!-- End Logo -->

                        <div class="card mb-3">

                            <div class="card-body">

                                <div class="pt-4 pb-2">
                                    <h5 class="card-title text-center pb-0 fs-4">Log In</h5>
                                    <p class="text-center small">Shaxsiy kabinetga kirish</p>
                                </div>

                                <form class="row g-3 needs-validation" method="post" action="{{route('login')}}">
                                    @csrf
                                    <div class="col-12">
                                        <label for="alogin" class="form-label">Login</label>
                                        <input type="text" name="alogin" class="form-control" id="alogin" required>
                                    </div>

                                    <div class="col-12">
                                        <label for="password" class="form-label">Parol</label>
                                        <input type="password" name="password" class="form-control" id="password" required>
                                    </div>

                                    <div class="col-12">
                                        <button class="btn btn-primary w-100" type="submit">Log in</button>
                                    </div>

                                    <div class="col-12">
                                        <p class="small mb-0">Don't have account? <a href="">Register</a></p>
                                    </div>

                                </form>

                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </section>

    </div>
</main><!-- End #main -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Vendor JS Files -->
<script src="https://style.softgo.uz/styles/assets/vendor/apexcharts/apexcharts.min.js"></script>
<script src="https://style.softgo.uz/styles/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="https://style.softgo.uz/styles/assets/vendor/chart.js/chart.min.js"></script>
<script src="https://style.softgo.uz/styles/assets/vendor/echarts/echarts.min.js"></script>
<script src="https://style.softgo.uz/styles/assets/vendor/quill/quill.min.js"></script>
<script src="https://style.softgo.uz/styles/assets/vendor/simple-datatables/simple-datatables.js"></script>
<script src="https://style.softgo.uz/styles/assets/vendor/tinymce/tinymce.min.js"></script>
<script src="https://style.softgo.uz/styles/assets/vendor/php-email-form/validate.js"></script>

<!-- Template Main JS File -->
<script src="https://style.softgo.uz/styles/assets/js/main.js"></script>

</body>

</html>
